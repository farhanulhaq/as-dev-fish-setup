#!/bin/bash

# install fish
apt-add-repository ppa:fish-shell/release-2 -y
apt-get update

# install essentials
apt-get install -y fish

echo "Setting up Access Server commandline aliases ..."
echo $PWD
mkdir -p ~/.config/fish/
cp as-dev-fish-setup/config.fish ~/.config/fish/
cp as-dev-fish-setup/exports.fish ~/
chsh -s /usr/bin/fish
as-dev-fish-setup/config.fish
fish

