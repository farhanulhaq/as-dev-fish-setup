set -x PYOVPN_VERSION 2
set -x OPENVPN_AS_BASE /usr/local/openvpn_as
set -x OPENVPN_AS_CONFIG /usr/local/openvpn_as/etc/as.conf
set -x PATH /usr/local/openvpn_as/scripts /usr/local/openvpn_as/bin /usr/local/openvpn_as/sbin $PATH
set -x LD_LIBRARY_PATH /usr/local/openvpn_as/lib
set -x LDFLAGS -L/usr/local/openvpn_as/lib
set -x CPPFLAGS -I/usr/local/openvpn_as/include
set -x PYTHONHOME /usr/local/openvpn_as
