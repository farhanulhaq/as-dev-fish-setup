# /usr/local/openvpn_as/scripts
alias confdba="/usr/local/openvpn_as/scripts/confdba"
alias sacli="/usr/local/openvpn_as/scripts/sacli"
alias liman="/usr/local/openvpn_as/scripts/liman"
alias liserv="/usr/local/openvpn_as/scripts/liserv"
alias ligeninit="/usr/local/openvpn_as/scripts/ligeninit"
alias likeygen="/usr/local/openvpn_as/scripts/likeygen"
alias licli="/usr/local/openvpn_as/scripts/licli"

# AS
alias as="cd /usr/local/openvpn_as"
alias as-log="cat /var/log/openvpnas.log"
alias as-stop="systemctl stop openvpnas"
alias as-start="systemctl start openvpnas"
alias as-status="systemctl status openvpnas"
alias t="tail -f -n 1500 /var/log/openvpnas.log"

# alias as-build="cd ~/as/as-build; and ./go"  # implemented as a function below
alias as-install="dpkg --install ~/dest/(ls -Art ~/dest | tail -n 1)"
alias as-remove="dpkg --purge openvpn-as; and rm -rf /usr/local/openvpn_as"
alias as-reinstall="as-remove; and as-install"
alias as-exports=". /usr/local/openvpn_as/exports.fish"

set -g theme_display_user yes
set -g theme_color_scheme terminal

function f
	 find ./ -name $argv
end

function g
	 if [ (count $argv) -eq 1 ]
	    grep -nrI $argv | \
	    awk -F: '{st=index($0, ":"); st2=index(substr($0, st+1), ":"); st3=substr($0, st+st2+1); sub(/^[ \t]+/, "", st3); x="\033[1m" NR "\033[34m " $1 ":" $2 "\033[0m" ; printf ("%s|%s\n", x, st3);  }' | \
	    column -s '|' -t | \
	    grep $argv
	 else
	    set x (grep -nrI $argv[1] | \
	    awk -F: '{st=index($0, ":"); st2=index(substr($0, st+1), ":"); printf "\033[1m" NR "\033[34m "$1":"$2": "; printf "\033[0m"; print substr($0, st+st2+1); }')
	    
            echo $x[$argv[2]] | awk '{print $2}' | awk -F':' '{print "e " $1":"$2 }' | fish			
	 end
end

function r
    rsync -Pav $argv[1] $argv[2]
end

